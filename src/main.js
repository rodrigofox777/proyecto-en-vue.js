import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate';
import VueRouter from 'vue-router';
import LastArticles from './components/LastArticles.vue';
import ListadoCiudadanos from './components/Ciudadano/ListadoCiudadanos.vue';
import ListadoTareas from './components/Tarea/ListadoTareas.vue';
import NuevoCiudadano from './components/Ciudadano/NuevoCiudadano.vue';
import EditCiudadano from './components/Ciudadano/EditCiudadano.vue';
import EditTarea from './components/Tarea/EditTarea.vue';
import Buscador from './components/Buscador.vue';
import Redirect from './components/Redirect.vue';
import Ciudadano from './components/Ciudadano/DetalleCiudadano.vue';
import Tarea from './components/Tarea/DetalleTarea.vue';
import NuevaTarea from './components/Tarea/NuevaTarea.vue';
import ErrorComponent from './components/ErrorComponent.vue';
Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(require('vue-moment'));
const routes = [
  {path: '/home', component:LastArticles},
  {path: '/ListaTareas', component:ListadoTareas},
  {path: '/editTarea/:id',name:'editTarea', component:EditTarea},
  {path: '/tarea/:id',name:'tarea', component:Tarea},
  {path: '/nuevaTarea', name:'nuevaTarea', component:NuevaTarea},
  {path: '/ListaCiudadanos', component:ListadoCiudadanos},
  {path: '/nuevoCiudadano', component:NuevoCiudadano},
  {path: '/editCiudadano/:id',name:'editCiudadano', component:EditCiudadano},
  {path: '/ciudadano/:id',name:'ciudadano', component:Ciudadano},
  {path: '/buscador/:ciudadanoNombre',component:Buscador},
  {path: '/redirect/:ciudadanoNombre',component:Redirect},
  {path: '/', component:LastArticles},
  {path: '*', component:ErrorComponent},
];

const router = new VueRouter({
  routes,
  mode:'history'
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

import HttpCliente from '../Service/HttpClient';

export const listarDias = () => {
    return new Promise((resolve) => {
        HttpCliente.get('DiaSemana/GetAll').then(response => {
            resolve(response);
        })
    });
}
import HttpCliente from '../Service/HttpClient';

export const insertTarea = tarea => {
    return new Promise((resolve) => {
        HttpCliente.post('Tarea/Insert', tarea).then(response => {
            resolve(response);
        })
    });
}


export const updateTarea = tarea => {
    return new Promise((resolve) => {
        HttpCliente.put('Tarea/Update', tarea).then(response => {
            resolve(response);
        })
    });
}

export const borrarTarea = tareaId => {
    return new Promise((resolve) => {
        HttpCliente.delete('Tarea/Delete/'+tareaId).then(response => {
            resolve(response);
        })
    });
}

export const listarTareas = () => {
    return new Promise((resolve) => {
        HttpCliente.get('Tarea/GetAll').then(response => {
            resolve(response);
        })
    });
}

export const listarTareasbyDay = diaId => {
    return new Promise((resolve) => {
        HttpCliente.get('Tarea/GetTareabyDay/'+diaId).then(response => {
            resolve(response);
        })
    });
}

export const obtenerTarea = tareaID => {
    return new Promise((resolve) => {
        HttpCliente.get('Tarea/Get/'+tareaID).then(response => {
            resolve(response);
        })
    });
}
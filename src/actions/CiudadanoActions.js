import HttpCliente from '../Service/HttpClient';

export const insertCiudadano = ciudadano => {
    return new Promise((resolve) => {
        HttpCliente.post('ciudadano/Insert', ciudadano).then(response => {
            resolve(response);
        })
    });
}

export const updateCiudadano = ciudadano => {
    return new Promise((resolve) => {
        HttpCliente.put('ciudadano/Update', ciudadano).then(response => {
            resolve(response);
        })
    });
}

export const listarCiudadanos = () => {
    return new Promise((resolve) => {
        HttpCliente.get('Ciudadano/GetAll').then(response => {
            resolve(response);
        })
    });
}

export const obtenerCiudadano = ciudadanoID => {
    return new Promise((resolve) => {
        HttpCliente.get('Ciudadano/Get/'+ciudadanoID).then(response => {
            resolve(response);
        })
    });
}

export const obtenerCiudadanobyNombre = ciudadanoNombre => {
    return new Promise((resolve) => {
        HttpCliente.get('Ciudadano/GetNombre/'+ciudadanoNombre).then(response => {
            resolve(response);
        })
    });
}

export const borrarCiudadano = ciudadanoID => {
    return new Promise((resolve) => {
        HttpCliente.delete('Ciudadano/Delete/'+ciudadanoID).then(response => {
            resolve(response);
        })
    });
}
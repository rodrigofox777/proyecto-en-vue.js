class Tarea{
    constructor(idTarea,nombre,descripcion,idDia,nroCiudadano){
        this.idTarea = idTarea;
        this.idDia = idDia;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.nroCiudadano = nroCiudadano;
    }
}

export default Tarea
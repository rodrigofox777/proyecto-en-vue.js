class Ciudadano{
    
    constructor(nroCiudadano,nombre,apellido,edad,genero){
        this.nroCiudadano = nroCiudadano;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.genero = genero;
    }
}

export default Ciudadano